<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Public+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styleexo12.css">
<style>

body {background-color:lightgrey;}

h2 {color:white;}

#container { display:flex;
            justify-content:space-between;
            width:70%;
            margin:auto;
            }

#container div {background-color:white;
                text-align:center;
                padding:50px;
                border-radius:10px ;
                box-shadow:0 0 5px 5px darkgrey;}

img {width:50px;
    height:50px;
      }

#container2 {text-align:center;
              background-color:white;
              width:70%;
              padding:10px;
              border-radius:10px;
              box-shadow:0 0 5px 5px darkgrey;
              margin: 40px auto;
              }

table {background-color:white;
      width:70%;
      border-radius:10px;
      box-shadow:0 0 5px 5px darkgrey;
      margin: 10px auto;}

tr:hover { background-color:grey;
color:white;
transition:background-color,1.5s;
transform:rotate (360deg);
}

td {padding:30px;

}

th {border-bottom:solid grey 4px;}
</style>

</head>    

    <?php
    
      /* 
    *   Simuler le tableau de bord d'une déchetterie grâce à la puissance de calcul de PHP et l'affichage du CSS
    *   Ici, votre tableau de bord, permettera de visualisé les stocks de dechets ( en tonne ) de la communauté de commune
    *   Trois catégories dans le tri : cartons, déchets verts, mobilier. La fonction rand affectera une quantité aléatoire aux  
    *   déchetteries du réseaux, à vous de faire en sorte que l'on sache qui à quoi et combien cela represente en tout. Préciser en 
    *   nombre de camion(s) le volume que cela represente. Un camion = 30 tonnes.
    *   Police utiliser : Public Sans sur Google Font, effet de ligne plus grise au survol de la ligne 
    */
    
    
    $lev = "Loir en vallée";
    $lcsll = "La Chartre-sur-le Loir";
    $bsd = "Beaumont-sur-Dême";
    $m = "Marçon";
    $c = "Chahaignes";
    $l = "Lhomme";
    
    $carton = rand(0,20);
    $vert = rand(0,20);
    $mobilier = rand(0,20);
    $totalcarton=$carton*6;
    $totalvert=$vert*6;
    $totalmobilier=$mobilier*6;    
    $totalcamion=round ($totalcarton/30);
    $totalcamion1=round ($totalvert/30);
    $totalcamion2=round ($totalmobilier/30);
    $supertotal=$totalcamion+$totalcamion1+$totalcamion2;

    ?>
   
    <!-- écrire le code après ce commentaire -->
    <h1>SICTOM de Montoire / La Chartre</h1>
    
    <h2>Quantité total de déchets dans la Communauté de communes</h2> 
    
 <div id=container>

    <div>
       <img src="carton.svg">
        <p> <?php echo $totalcarton;?> </p>
        <h4> Besoin de <?php echo $totalcamion;?> camion(s)</h4>
    </div>

    <div>
      <img src="vert.svg">
        <p> <?php echo $totalvert;?> </p>
        <h4> Besoin de <?php echo $totalcamion1;?> camion(s)</h4>
    </div>

    <div>
      <img src="mobilier.svg">
        <p> <?php echo $totalmobilier;?></p>
        <h4> Besoin de <?php echo $totalcamion2 ;?> camion(s)</h4>
    </div>
</div>

<div id=container2>
      <p> Nombre de camions total à prévoir: <?php echo $supertotal;?> </p>

</div>

<table cellspacing=0>
<thead>
<tr>
<th>Sites de collecte</th>
<th><img src="carton.svg"></th>
<th><img src="vert.svg"></th>
<th><img src="mobilier.svg"></th>
</tr>
</thead>

<tbody>
<tr>
  <td>Loir en vallée</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
<tr>
  <td>La chartre-sur-le-loir</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
<tr>
  <td>Beaumont-sur-Dême</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
<tr>
  <td>Marçon</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
<tr>
  <td>Chahaighes</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
<tr>
  <td>Lhomme</td>
  <td><?php echo $carton;?></td>
  <td><?php echo $vert;?></td>
  <td><?php echo $mobilier;?></td>
</tr>
</tbody>

</table>

    <!-- écrire le code avant ce commentaire -->

</body>
</html>

